package com.lab;



public class Busqueda {

	static Integer[] arreglo = { 2, 5, 9, 58, 36, 78, 84 };

	static Integer clave =5;

	public int secuencial() {
		int resultado = -1;

		for (int i = 0; i < arreglo.length; i++) {
			if (arreglo[i] == clave) {
				resultado = i;
				break;
			}
		}

		return resultado;
	}
	
	public static int binaria() {
		int resultado = -1;
		ordenarLista();
		int puntoCentral = (arreglo.length -1) / 2;
			
		if(arreglo[puntoCentral] == clave)
		{
			resultado = puntoCentral;
		}
		
		if(arreglo[puntoCentral] < clave)
		{
			for(int i=puntoCentral+1; i<arreglo.length; i++)
			{
				if(arreglo[i] == clave)
				{
					resultado=i;
				}
			}
		}
		else
		{
			for(int i=0; i<puntoCentral; i++)
			{
				if(arreglo[i] == clave)
				{
					resultado=i;
				}
			}
		}
					
		return resultado;
	}
	
	public static void ordenarLista() {
		int menor;
		int posMenor=0;			
		String ctrl;
	//{ 50, 20, 40, 80, 30 };
	for (int i=0; i<arreglo.length;i++)
	{
		ctrl="No";
		menor = arreglo[i];
		
		for(int j=i+1; j<arreglo.length;j++)
		{
			if(arreglo[j] < menor)
			{
				posMenor= j;
				menor=arreglo[j];
				ctrl ="Si";
			}				
		}
		
		if(ctrl.equals("Si")) {
			arreglo[posMenor]=arreglo[i];
			arreglo[i]=menor;				
		}								
	}
}
	
	public static void main(String[] args) {
		Busqueda busqueda = new Busqueda();
		
		int posicion = busqueda.binaria();
		
		if(posicion == -1) {
			System.out.println("Valor no encontrado");
		}else {
			System.out.println("El valor clave " + clave + " encontrado en posicion " + posicion );
		}
	}
}
