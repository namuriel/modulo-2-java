package com.lab;

public class Ordenamiento {

	public int[] numeros = { 50, 20, 40, 80, 30 };

	public void imprimir() {
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("Arreglo posicion " + i + " valor " + numeros[i]);
		}
	}

	public void seleccion() {
		int i, j;
		int indiceMenor;

		for (i = 0; i < numeros.length - 1; i++) {

			indiceMenor = i;

			for (j = i + 1; j < numeros.length; j++) {

				if (numeros[j] < numeros[indiceMenor]) {

					indiceMenor = j;
				}
			}

			if (i != indiceMenor) {
				int aux = numeros[i];
				numeros[i] = numeros[indiceMenor];
				numeros[indiceMenor] = aux;
			}
		}
	}

	public void burbuja() {
		int pasada, i;

		for (pasada = 0; pasada < numeros.length - 1; pasada++) {

			for (i = 0; i < numeros.length - 1 - pasada; i++) {

				if (numeros[i] > numeros[i + 1]) {
					int aux = numeros[i];
					numeros[i] = numeros[i + 1];
					numeros[i + 1] = aux;
				}

			}

		}
	}

	public void insercion() {
			int menor;
			int posMenor=0;			
			String ctrl;	
		for (int i=0; i<numeros.length;i++)
		{
			ctrl="No";
			menor = numeros[i];
			
			for(int j=i+1; j<numeros.length;j++)
			{
				if(numeros[j] < menor)
				{
					posMenor= j;
					menor=numeros[j];
					ctrl ="Si";
				}				
			}
			
			if(ctrl.equals("Si")) {
				numeros[posMenor]=numeros[i];
				numeros[i]=menor;				
			}								
		}
	}

	public static void main(String[] args) {

		Ordenamiento ord = new Ordenamiento();
		System.out.print("Lista orignal: \n");
		ord.imprimir();

		ord.insercion();

		System.out.print("\n Lista Ordenada: \n");
		ord.imprimir();

	}
}
