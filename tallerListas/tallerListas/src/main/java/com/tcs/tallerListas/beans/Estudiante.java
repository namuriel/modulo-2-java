package com.tcs.tallerListas.beans;

public class Estudiante {
    private String documento;
    private String nombre;
    private int edad;
    private String grado;
    private String repiteGrado;

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;   
    }
    
    public String getRepiteGrado() {
        return repiteGrado;
    }

    public void setRepiteGrado(String repite) {
        this.repiteGrado = repite;   
    }
    
    
}
