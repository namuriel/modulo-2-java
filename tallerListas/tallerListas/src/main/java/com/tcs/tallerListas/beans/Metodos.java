package com.tcs.tallerListas.beans;

import java.util.ArrayList;

public class Metodos {
	ArrayList<Estudiante> listaCompleta = new ArrayList<Estudiante>();
	ArrayList<Estudiante> estudiantesJardin = new ArrayList<Estudiante>();
	ArrayList<Estudiante> estudiantesPrimero = new ArrayList<Estudiante>();
	ArrayList<Estudiante> estudiantesSegundo = new ArrayList<Estudiante>();
	ArrayList<Estudiante> estudiantesTercero = new ArrayList<Estudiante>();
	ArrayList<Estudiante> estudiantesCuarto = new ArrayList<Estudiante>();
	ArrayList<Estudiante> estudiantesQuinto = new ArrayList<Estudiante>();
	
	public void llenarListaCompleta(String pDocumento,String pNombre, int pEdad, String pGrado, String pRepite)	
	{
		Estudiante objEstudiante = new Estudiante();
		objEstudiante.setDocumento(pDocumento);
		objEstudiante.setNombre(pNombre);
		objEstudiante.setEdad(pEdad);
		objEstudiante.setGrado(pGrado);
		objEstudiante.setRepiteGrado(pRepite);
		
		listaCompleta.add(objEstudiante);
	}
	
	public boolean convertirEdad(String pEdad)
	{
	 try {
            Integer.parseInt(pEdad);
            return true;           
        } catch (NumberFormatException error) {
        	
        	System.out.print("\n �* UN VALOR INGRESADO EN EL CAMPO EDAD NO ES NUMERICO, POR FAVOR VALIDAR EL ARCHIVO Y CORREGIR EL DATO *! \n");
            return false;
        }
	}
	
	public int cantidadTotal()
	{
		return listaCompleta.size();
	}
	
	
	public int cantidadPorEdad()
	{
		int contador = 0;
		
		for(int i=0; i < listaCompleta.size(); i++)
		{
			if(listaCompleta.get(i).getEdad() > 5 && listaCompleta.get(i).getEdad() < 11)
			{
				contador += 1;
			}
		}
		return contador;
	}
	
	public void imprimirResultados()
	{
		System.out.print("\n El n�mero total de estudiantes es: " + cantidadTotal() + ("\n"));
		System.out.print("\n El n�mero total de estudiantes con edades entre los 6 y 10 a�os son: " + cantidadPorEdad() + ("\n"));
		imrpimirListaPorGrado();			
	}
	
	public void estudiantePorGrado()
	{
		for(int i=0; i < listaCompleta.size(); i++)
		{
			Estudiante objEstudiante = new Estudiante();
			
			switch (listaCompleta.get(i).getGrado()) {
			case "Jardin":
				objEstudiante.setDocumento(listaCompleta.get(i).getDocumento());
				objEstudiante.setEdad(listaCompleta.get(i).getEdad());
				objEstudiante.setGrado("Jardin");
				objEstudiante.setNombre(listaCompleta.get(i).getNombre());
				objEstudiante.setRepiteGrado(listaCompleta.get(i).getRepiteGrado());
				
				estudiantesJardin.add(objEstudiante);
				break;
				
			case "Primero":
				objEstudiante.setDocumento(listaCompleta.get(i).getDocumento());
				objEstudiante.setEdad(listaCompleta.get(i).getEdad());
				objEstudiante.setGrado("Primero");
				objEstudiante.setNombre(listaCompleta.get(i).getNombre());
				objEstudiante.setRepiteGrado(listaCompleta.get(i).getRepiteGrado());
				
				estudiantesPrimero.add(objEstudiante);
				break;
				
			case "Segundo":
				objEstudiante.setDocumento(listaCompleta.get(i).getDocumento());
				objEstudiante.setEdad(listaCompleta.get(i).getEdad());
				objEstudiante.setGrado("Segundo");
				objEstudiante.setNombre(listaCompleta.get(i).getNombre());
				objEstudiante.setRepiteGrado(listaCompleta.get(i).getRepiteGrado());
				
				estudiantesSegundo.add(objEstudiante);
				break;
				
			case "Tercero":
				objEstudiante.setDocumento(listaCompleta.get(i).getDocumento());
				objEstudiante.setEdad(listaCompleta.get(i).getEdad());
				objEstudiante.setGrado("Tercero");
				objEstudiante.setNombre(listaCompleta.get(i).getNombre());
				objEstudiante.setRepiteGrado(listaCompleta.get(i).getRepiteGrado());
				
				estudiantesTercero.add(objEstudiante);
				break;
				
			case "Cuarto":
				objEstudiante.setDocumento(listaCompleta.get(i).getDocumento());
				objEstudiante.setEdad(listaCompleta.get(i).getEdad());
				objEstudiante.setGrado("Cuarto");
				objEstudiante.setNombre(listaCompleta.get(i).getNombre());
				objEstudiante.setRepiteGrado(listaCompleta.get(i).getRepiteGrado());
				
				estudiantesCuarto.add(objEstudiante);
				break;
				
			case "Quinto":
				objEstudiante.setDocumento(listaCompleta.get(i).getDocumento());
				objEstudiante.setEdad(listaCompleta.get(i).getEdad());
				objEstudiante.setGrado("Quinto");
				objEstudiante.setNombre(listaCompleta.get(i).getNombre());
				objEstudiante.setRepiteGrado(listaCompleta.get(i).getRepiteGrado());
				
				estudiantesQuinto.add(objEstudiante);
				break;

			default:
				break;
			}
		}
	}
	
	public void imrpimirListaPorGrado()
	{
		if(estudiantesJardin.size() > 0)			
		{
			imprimirEstudiantePorGrado(estudiantesJardin, "\n LOS ESTUDIANTES DE JARDIN SON: \n");
		}
		
		if(estudiantesPrimero.size() > 0)
		{
			imprimirEstudiantePorGrado(estudiantesPrimero, "\n LOS ESTUDIANTES DE PRIMERO SON: \n");
		}
		
		if(estudiantesSegundo.size() > 0)
		{
			imprimirEstudiantePorGrado(estudiantesSegundo, "\n LOS ESTUDIANTES DE SEGUNDO SON: \n");
		}
		
		if(estudiantesTercero.size() > 0)
		{
			imprimirEstudiantePorGrado(estudiantesTercero, "\n LOS ESTUDIANTES DE TERCERO SON: \n");
		}
		
		if(estudiantesCuarto.size() > 0)
		{
			imprimirEstudiantePorGrado(estudiantesCuarto, "\n LOS ESTUDIANTES DE CUARTO SON: \n");
		}
		
		if(estudiantesQuinto.size() > 0)
		{
			imprimirEstudiantePorGrado(estudiantesQuinto, "\n LOS ESTUDIANTES DE QUINTO SON: \n");		
		}
	}
	
	public void estudianteConTi()
	{
		System.out.print("\nESTUDIANTES CON IDENTIFICACION TI: \n");
		
		//Recorrido con Expresion Lambda
		listaCompleta.forEach(identificacion ->{
			if(identificacion.getDocumento().startsWith("TI"))
			{
				System.out.print(identificacion.getEdad() + "," + identificacion.getGrado() + "\n");
			}
		});		
	}
	
	public void repitenGrado()
	{
		System.out.print("\nESTUDIANTES QUE REPITEN GRADO:\n");
		
		//Recorrido con Expresion Lambda
		listaCompleta.forEach(repiten ->{
			if(repiten.getRepiteGrado().equals("Repite"))
			{ 
				System.out.print(repiten.getNombre() + "\n");
			}
		});
		
		//Recorrido tradicional For
//		for(int i=0; i < listaCompleta.size(); i++)
//		{
//			if(listaCompleta.get(i).getRepiteGrado().equals("Repite"))
//			{
//				System.out.print(listaCompleta.get(i).getNombre() + "\n");
//			}
//		}
	}
	
	public void imprimirEstudiantePorGrado(ArrayList<Estudiante> lista, String mensaje)
	{
		System.out.print(mensaje);		
		lista.forEach(estudiante ->{System.out.print(estudiante.getDocumento() + "," + estudiante.getNombre() + "," + estudiante.getEdad() + "\n");
		});			
	}	
}
